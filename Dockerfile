FROM puckel/docker-airflow:1.10.8
RUN pip install --user psycopg2-binary
ENV AIRFLOW_HOME=/usr/local/airflow
COPY ./airflow.cfg /usr/local/airflow/airflow.cfg
COPY ./ /usr/local/staffing_dashboard/

USER root
RUN chmod -R 775 /usr/local/staffing_dashboard/
RUN chown -R airflow:airflow /usr/local/staffing_dashboard/

RUN rm -rf /usr/local/airflow/dags/
RUN mkdir -p -m 775 /usr/local/airflow/dags
RUN chown -R airflow:airflow /usr/local/airflow/dags

RUN mkdir -p -m 775 /usr/local/airflow/logs
RUN chown -R airflow:airflow /usr/local/airflow/logs


USER airflow
COPY ./dags/ /usr/local/airflow/dags/
RUN ls -la /usr/local/airflow/dags/
RUN pip install /usr/local/staffing_dashboard/. --user

ENV PATH_TO_CREDS="/usr/local/staffing_dashboard/creds/"
ENV PATH_TO_DATA="/usr/local/staffing_dashboard/data/"

